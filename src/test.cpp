#include "situation.hpp"
#include "voiture.hpp"
#include <vector>
#include "situation_dynamique.hpp"

struct Arretes {
	int deplacement;
	int ind_voiture;
};

static bool appartient(Situation s,std::vector<Situation> vect){
	for(int i=0;i<vect.size();i++){
		if(vect[i]==s)
			return true;
	}
	return false;
}

int main(int argc ,char* argv[]){
	if (argc <2){
		printf( "mauvaise utilisation: %s <fichier_entrée> \n",argv[0]);
	}
	//plusieurs "seed" avec une solution, "assez rapidement" étant donné que cela peut boucler très longtemps ..
	//ordre croissant en fonction du nombre de coups réalisés :
	//661132266
	//6611332266
	//661144332266
	//66114332266
	//6611226
	srand(661132266);
	printf("init grace au fichier : %s \n",argv[1]);
	Situation_dynamique Situation_courrante (argv[1]);
	Situation_courrante.sit.afficher();
	//on creer les vecteurs qui nous serviront de graphes pour le moment
	std::vector<Situation> vecteur_situation;
	vecteur_situation.push_back(Situation_courrante.sit);
	std::vector<Arretes> vecteur_arretes;
	
	bool solution_trouvee=Situation_courrante.sit.jeu_fini();
	
	
	while (solution_trouvee==false){
		// A DECOMMENTER
		//bool nouvelle_situation_deja_dans_graphe=true;
		std::vector<std::vector<int>> liste_liste_deplacement_possible; //va contenir des vecteurs d'entiers, qui representeront les deplacements possible pour chacune des voitures présente
		std::vector<int> voiture_eligible_tirage; // contient les indices des voitures qui peuvent se deplacer : vecteur de déplacement non vide
		
		int ind_voiture_alea,val_deplacement_alea;
		Situation nouvelle_situation;
	
		//on rempli nos deux vecteurs précédemment déclarés :
		for(int i=0;i<Situation_courrante.sit.card;i++){
			std::vector<int> liste_dep_pour_cette_voiture=Situation_courrante.liste_dep_possible(i);
			liste_liste_deplacement_possible.push_back(liste_dep_pour_cette_voiture);
			if (liste_dep_pour_cette_voiture.size()!=0)
				voiture_eligible_tirage.push_back(i);
		}
		bool reste_voiture_eligible=(voiture_eligible_tirage.size()!=0);
		
		//A DECOMMENTER
		while (/*nouvelle_situation_deja_dans_graphe && */reste_voiture_eligible) {
			
			// on prend une voiture au hasard parmis les voiture pouvant se déplacer :
			int ind_voiture_eligible_alea=rand()%(voiture_eligible_tirage.size());
			ind_voiture_alea =voiture_eligible_tirage[ind_voiture_eligible_alea];
			
			//on choisit une valeure de déplacement aléatoire également:
			int indice_deplacement_alea=rand()%(liste_liste_deplacement_possible[ind_voiture_alea].size());
			val_deplacement_alea= liste_liste_deplacement_possible[ind_voiture_alea][indice_deplacement_alea];
			
			//on genère la situation associé au déplacement choisi :
			nouvelle_situation=Situation_courrante.generer_situation(ind_voiture_alea,val_deplacement_alea);
			//on eneleve la valeur de déplacement aleatoire des deplacements possible, au cas ou il faille tester un autre deplacement
			liste_liste_deplacement_possible[ind_voiture_alea].erase(liste_liste_deplacement_possible[ind_voiture_alea].begin()+indice_deplacement_alea);
			//si on enlève le dernier deplacement possible pour une voiture il faut spécifier qu'elle n'est plus déplacable
			if(liste_liste_deplacement_possible[ind_voiture_alea].size()==0)
				voiture_eligible_tirage.erase(voiture_eligible_tirage.begin()+ind_voiture_eligible_alea);
			//on regarde si la nouvelle situation est dans notre graphe : fonction recherche qui n'est pas optimale au niveau complexité : plus on va ajouter d'élément dans notre vecteur plus la ocmplexité augmente , la taille du vecteur est borné donc ok, mais on peut faire mieux
			
			// A DECOMMENTER 
			//nouvelle_situation_deja_dans_graphe=appartient(nouvelle_situation,vecteur_situation);
			
			//on regarde s'il reste des coups possible a jouer, au cas ou le coup joué donne une situation deja présente dans le graphe
			reste_voiture_eligible=(voiture_eligible_tirage.size()!=0);
		}
		//ici on a soit une nouvelle situation a &jouter au graphe et on recommence si le jeu n'est pas fini, soit la situation est déjà dans le graphe et il n'y a pas d'autre possibilités de déplacements qui n'ont pas été testé
		
		//si il n'y a plus de possibilité a partir de la situation courrante et que la situation calculé en dernier est déja dans notre graphe, on renvoie un echec
		/*if(nouvelle_situation_deja_dans_graphe && !reste_voiture_eligible ){
			std::cout<<"impossible de trouver une solution .."<<std::endl;
			return 0;
		}*/
		
		//si la situation peut etre ajouté, on met a jour notre situation dynamique et on regarde si le jeu est terminé
		//else {
			std::cout<<"on va deplacer la voiture "<<ind_voiture_alea<<" de "<<val_deplacement_alea<<" cases ..."<<std::endl;
			Situation_courrante.sit=nouvelle_situation;
			
			nouvelle_situation.afficher();
			Situation_courrante.maj_terrain();
			vecteur_situation.push_back(Situation_courrante.sit);
			//on creer une nouvelle arrete
			Arretes nouvelle_arrete;
			nouvelle_arrete.deplacement=val_deplacement_alea;
			nouvelle_arrete.ind_voiture=ind_voiture_alea;
			//on ajoute cette nouvelle arrete
			vecteur_arretes.push_back(nouvelle_arrete);
			//on regarde si le jeu est fini
			solution_trouvee=Situation_courrante.sit.jeu_fini();
			std::cout<<"-------------------------------------------------"<<std::endl;
		//}
	}
	std::cout<<"solution trouvée !!!!!!!!!!!!!!"<<std::endl;
	std::cout<<"nombre de coups : "<<vecteur_arretes.size()<<std::endl;
	return 0;
}

