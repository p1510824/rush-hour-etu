#ifndef _SITUATION_DYNAMIQUE_HPP_
#define _SITUATION_DYNAMIQUE_HPP_

#include "situation.hpp"
#include "vector"
/* idée: on a dans le main une situation dite dynamique qui permet :
	de stocker un tableau de booléen qui evolué selon les positions des voitures de base, et qui suit les deplacements -> temps constant pour savoir si deplacement possible et algo simple pur liste dep possible
	de stocker nos situations "basique" dans un vecteur pour retrouver la trace des coups joué, car il n'y pas a pas beaucoup d'informations dans une situation basique, donc pas beaucoup de mémoire utilisé quand on les stocke
	
	
*/
class Situation_dynamique { 
	public:
	Situation sit;
	char terrain[TAILLE_TERRAIN][TAILLE_TERRAIN];// terrain[y][x] <=> x = la colonne, y = la ligne
	
	Situation_dynamique(char* nom_fichier); // initialise une situation dynamique (qui est amené a évoluer selon les coups joué par la suite) , selon un fichier donné
	
	bool dep_possible(int ind_voiture,int val_deplacement); // dit si le deplacement est possible ou non, valeur du depalcement + ou - selon si on va a droite ou a gauche / en bas ou en haut
	
	std::vector<int> liste_dep_possible (int ind_voiture); // contient des entiers , exemple : 1 signifie que la voiture peut aller a droite / en bas selon son orientation d'une case en avant, 2 idem mais dans deux cases dans ce sens
	
	Situation generer_situation(int voiture,int deplacement); // joue un coup sur la situation courrante : deplace la voiture indiqué du nombre de case indiqué
	
	void maj_terrain();
	
};

#endif