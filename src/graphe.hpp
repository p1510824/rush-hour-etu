#ifndef _GRAPHE_HPP_
#define _GRAPHE_HPP_

#include "situation_dynamique.hpp"
#include "sommet.hpp"

class Graphe {
	public:
	std::vector<Sommet> vect_sommet;// représente l'ensemble des sommets de notre graphe, le sommet "initial" étant à l'indice 0
	
	void ajouter(Sommet s);
	int rechercher(Sommet s);
	int rechercher_pred(int ind_sommet);

};
#endif