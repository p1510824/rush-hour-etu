#ifndef _ARRETE_HPP_
#define _ARRETE_HPP_

#include "situation_dynamique.hpp"

class Arrete {
	public:
	int ind_suivant; // représente l'indince dans le vecteur ou se trouve le sommet suivant que l'on atteint
	int val_dep; // représente la valeur du déplacement a effectuer pour atteindre le suivant mentionné
	int ind_voiture; //représente la voiture qu'il faut bouger pour atteindre le suivant mentionné
	
	Arrete(int ind_s,int val,int ind_v);
};
#endif