#include "situation.hpp"
#include "voiture.hpp"
#include <vector>
#include <queue>
#include "situation_dynamique.hpp"
#include "graphe.hpp"


int main(int argc ,char* argv[]){
	if (argc <2){
		printf( "mauvaise utilisation: %s <fichier_entrée> \n",argv[0]);
	}
	printf("init grace au fichier : %s \n",argv[1]);
	Situation_dynamique Situation_courrante (argv[1]);
	Situation_courrante.sit.afficher();
	//on creer notre graphe puis notre vecteur d'indice, qui représente les sommets dont il faut calculer les suivants :
	std::queue<int> liste_sommet_a_calculer;
	Graphe G;
	G.ajouter(Sommet(Situation_courrante.sit));
	liste_sommet_a_calculer.push(0);
	bool solution_trouvee=Situation_courrante.sit.jeu_fini();
	int ind_sommet_actuel;
	
	while (solution_trouvee==false && liste_sommet_a_calculer.size()!=0 ){
		std::cout<<"on calcule l'ensembe des sommets accessible depuis le sommet actuel"<<std::endl;
		ind_sommet_actuel=liste_sommet_a_calculer.front();
		std::vector<std::vector<int>> liste_liste_deplacement_possible; //va contenir des vecteurs d'entiers, qui representeront les deplacements possible pour chacune des voitures présente
		std::vector<int> voiture_deplacable; // contient les indices des voitures qui peuvent se deplacer : vecteur de déplacement non vide
		Situation nouvelle_situation;
		Situation_courrante.sit=G.vect_sommet[ind_sommet_actuel].valeur; //
		Situation_courrante.maj_terrain();
		//on rempli nos deux vecteurs précédemment déclarés :
		for(int i=0;i<Situation_courrante.sit.card;i++){
			int ind_voit=i;
			std::vector<int> liste_dep_pour_cette_voiture=Situation_courrante.liste_dep_possible(i);
			liste_liste_deplacement_possible.push_back(liste_dep_pour_cette_voiture);
			
			if (liste_dep_pour_cette_voiture.size()!=0){
				voiture_deplacable.push_back(ind_voit);
			}
		}
		//for(int i=0;i<voiture_deplacable.size();i++)
			//std::cout<<i<<std::endl;
		// on creer maintenant les sommets atteignable depuis le sommet courrant 
		bool reste_voiture_deplacable=(voiture_deplacable.size()!=0);
		int val_deplacement,ind_voiture;
		
		
		while ( reste_voiture_deplacable) {
			// on prend une voiture parmis les voiture pouvant se déplacer :
			ind_voiture =voiture_deplacable[voiture_deplacable.size()-1];
			//on choisit une valeure de déplacement :
			val_deplacement= liste_liste_deplacement_possible[ind_voiture][liste_liste_deplacement_possible[ind_voiture].size()-1];
			//on genère la situation associé au déplacement choisi :
			nouvelle_situation=Situation_courrante.generer_situation(ind_voiture,val_deplacement);
			
			//on eneleve la valeur de déplacement utilisé
			liste_liste_deplacement_possible[ind_voiture].pop_back();
			//si on enlève le dernier deplacement possible pour une voiture il faut spécifier qu'elle n'est plus déplacable
			if(liste_liste_deplacement_possible[ind_voiture].size()==0){
				voiture_deplacable.pop_back();
			}
			//on creer le nouveau sommet 
			Sommet nouveau_sommet(nouvelle_situation);
			//on regarde maintenant si le sommet calculé est dans le graphe, on obtient alors son indice s'il y est, on peut créer l'arrete qui permet d'aller de notre sommet actuel a ce nouveau sommet
			// s'il n'y est pas on ajoute le nouveau sommet au graphe, son indice sera alors l'indice du dernier element du graphe, et on peut donc creer l'arrete vers ce nouveau sommet egalement
			int indice_nouveau_sommet=G.rechercher(nouveau_sommet);
			if (indice_nouveau_sommet== -1) { //sommet non trouvé dans le graphe
				std::cout<<"on ajoute un sommet dans le graphe"<<std::endl;
				G.vect_sommet.push_back(nouveau_sommet);
				indice_nouveau_sommet=G.vect_sommet.size()-1;
				Arrete a(indice_nouveau_sommet,val_deplacement,ind_voiture);
				G.vect_sommet[ind_sommet_actuel].suivant.push_back(a);
				//on oublie pas qu'il faudra calculer les sommets accessible depuis ce nouveau sommet plus tard :
				liste_sommet_a_calculer.push(indice_nouveau_sommet);
			}
			else {
				std::cout<<"sommet deja dans le graphe"<<std::endl;
				Arrete a(indice_nouveau_sommet,val_deplacement,ind_voiture);
				G.vect_sommet[ind_sommet_actuel].suivant.push_back(a);
			}
			reste_voiture_deplacable=(voiture_deplacable.size()!=0);
		}
		//on a fini avec le sommet actuel:
		liste_sommet_a_calculer.pop();
		if(Situation_courrante.sit.jeu_fini()==true)
			solution_trouvee=Situation_courrante.sit.jeu_fini();
	}
	std::cout<<"le graphe a été calculé completement"<<std::endl;
	std::cout<<G.vect_sommet.size()<<std::endl;
	std::vector<int> chemin;
	//
	int ind_sommet_parcours=ind_sommet_actuel;
	while(ind_sommet_parcours!= 0){
		chemin.push_back(ind_sommet_parcours);
		int ind_pred=G.rechercher_pred(ind_sommet_parcours);
		ind_sommet_parcours=ind_pred;
	}
	chemin.push_back(ind_sommet_parcours);
	for(int i=chemin.size()-1;i>=0;i--){
		G.vect_sommet[chemin[i]].valeur.afficher();
	}
	
	std::cout<<"nombre de coups réalisés : "<<chemin.size()<<std::endl;
	
	return 0;
}

