#include "situation_dynamique.hpp"

void Situation_dynamique::maj_terrain(){
	for(int i=0;i<TAILLE_TERRAIN;i++)
		for(int j=0;j<TAILLE_TERRAIN;j++)
			terrain[i][j]=false;
	for(int i=0;i<sit.card;i++){//pour chaque voiture 
		Voiture v;
		v.egal(sit.tab_voiture[i]);
		if (v.orientation==HORIZONTALE)
			for(int i=v.c;i<v.c+v.longueur;i++)
				terrain[v.l][i]=true;

		else
			for(int i=v.l;i<v.l+v.longueur;i++)
				terrain[i][v.c]=true;
		
	}
}

Situation_dynamique::Situation_dynamique(char* nom_fichier){
	sit=Situation(nom_fichier);
	this->maj_terrain();
	std::cout<<"tableau booleen"<<std::endl;
	for(int i=0;i<TAILLE_TERRAIN;i++){
		for(int j=0;j<TAILLE_TERRAIN;j++){
			if(terrain[i][j])
				std::cout<<"1 ";
			else 
				std::cout<<"0 ";
		}
		std::cout << " "<<std::endl;
	}
		
}

bool Situation_dynamique::dep_possible(int ind_voiture,int val_deplacement){// RAPPEL : deplacement vers le haut / a gauche -> valeur négative, vers la droite / en bas -> valeur positive
	//test sur les entrées pour éviter toutes surprises :
	if(val_deplacement==0||ind_voiture<0||ind_voiture>=sit.card){
		std::cout<<"ATTENTION: erreurs sur les entrées dans la fonction 'dep_possible'"<<std::endl;
		return false;
	}
	if(val_deplacement>0){ 

		if (sit.tab_voiture[ind_voiture].orientation==HORIZONTALE){
			//on est dans le cas d'une voiture horizontale qui veut avancer de x case a droite
			//si le déplacement fait sortir du cadre : impossible
			if(sit.tab_voiture[ind_voiture].c+sit.tab_voiture[ind_voiture].longueur-1+val_deplacement>=TAILLE_TERRAIN){
				return false;
			}
			//on regarde maintenant si par exemple on vetu avancer de 3 cases, s'il est deja possible d'avancer de 1 et de 2 cases, si ce n'est pas le cas il ne sera pas possible d'avancer de 3 cases :
			//à chaque passage dans la boucle, on regarde dans notre tableau de booléen si une voiture occupe les cases qui séparent l'avant de notre voiture de sa case "de destination"
			//après avoir passé la boucle for suivante, nous avons la garanti que la voiture n'a pas d'obstacle entre elle et la case de "destination", sera utile dans le cas ou l'on veut faire des deplacements de plusieurs cases en un seul coup.
			for(int deplacement=1; deplacement<val_deplacement;deplacement++){
				if (terrain[sit.tab_voiture[ind_voiture].l][sit.tab_voiture[ind_voiture].c+sit.tab_voiture[ind_voiture].longueur-1+deplacement]){
					return false;
				}
			}
			//on renvoie maintenant si la case destination est occupée ou non :
			return (!terrain[sit.tab_voiture[ind_voiture].l][sit.tab_voiture[ind_voiture].c+sit.tab_voiture[ind_voiture].longueur-1+val_deplacement]);
		}
		else if (sit.tab_voiture[ind_voiture].orientation==VERTICALE){
			//on est dans le cas d'une voiture verticale qui veut avancer de x case vers le bas
			//si le déplacement fait sortir du cadre : impossible
			if(sit.tab_voiture[ind_voiture].l+sit.tab_voiture[ind_voiture].longueur-1+val_deplacement>=TAILLE_TERRAIN){
				return false;
			}
			for(int deplacement=1; deplacement<val_deplacement;deplacement++){
				if (terrain[sit.tab_voiture[ind_voiture].l+sit.tab_voiture[ind_voiture].longueur-1+deplacement][sit.tab_voiture[ind_voiture].c]){
					return false;
				}
			}
			return (!terrain[sit.tab_voiture[ind_voiture].l+sit.tab_voiture[ind_voiture].longueur-1+val_deplacement][sit.tab_voiture[ind_voiture].c]);
		}
	}
	else {
		
		//depuis la position "de base" de la voiture on regarde de la même manière à x cases derrière
		if (sit.tab_voiture[ind_voiture].orientation==HORIZONTALE){
			//on est dans le cas d'une voiture horizontale qui veut avancer de x case a gauche
			//si le déplacement fait sortir du cadre : impossible
			if(sit.tab_voiture[ind_voiture].c+val_deplacement<0){
				return false;
			}
			for(int deplacement=-1; deplacement>val_deplacement;deplacement--){
				if (terrain[sit.tab_voiture[ind_voiture].l][sit.tab_voiture[ind_voiture].c+deplacement])
					return false;
			}
			return (!terrain[sit.tab_voiture[ind_voiture].l][sit.tab_voiture[ind_voiture].c+val_deplacement]);
		}
		else if (sit.tab_voiture[ind_voiture].orientation==VERTICALE){
			//on est dans le cas d'une voiture horizontale qui veut avancer de x case a droite
			//si le déplacement fait sortir du cadre : impossible
			if(sit.tab_voiture[ind_voiture].l+val_deplacement<0){
				return false;
			}
			for(int deplacement=-1; deplacement>val_deplacement;deplacement--){
				if (!terrain[sit.tab_voiture[ind_voiture].l+deplacement][sit.tab_voiture[ind_voiture].c]){
					return false;
				}
			}
			return(!terrain[sit.tab_voiture[ind_voiture].l+val_deplacement][sit.tab_voiture[ind_voiture].c]);
		}
	}
}

std::vector<int> Situation_dynamique::liste_dep_possible (int ind_voiture){//renvoie un vecteur contenant les valeurs de déplacement possible
	std::vector<int> liste_deplacements_possible;
	//on va tester tout les deplacements "positifs" en partant d'un déplacement de 1 case, on s'arrete a partir du moment ou un deplacement n'est pas possible
	for(int deplacement=1;deplacement<TAILLE_TERRAIN;deplacement++){
		if(dep_possible(ind_voiture,deplacement)){
			liste_deplacements_possible.push_back(deplacement);
		}
		else
			break;
	}
	//on test desormais les deplacements negatifs :
	for(int deplacement=-1;deplacement>(-TAILLE_TERRAIN);deplacement--){
		if(dep_possible(ind_voiture,deplacement)){
			liste_deplacements_possible.push_back(deplacement);
		}
		else
			break;
	}
	//afficher deplacements possible :
	/*
	std::cout<<"deplacements possible pour voiture "<<char('a'+ind_voiture) <<"  :"<<std::endl;
	for(int i=0;i<liste_deplacements_possible.size();i++)
		std::cout<<liste_deplacements_possible[i]<<" ";
	std::cout<< " " <<std::endl;
	*/
	
	return liste_deplacements_possible;
} 


	
Situation Situation_dynamique::generer_situation(int voiture,int deplacement){
	//cette fonction sera appelé uniquement si le déplacement est possible : aucune verrification nécessaire
	Situation nouvelle_situation=sit;
	if (nouvelle_situation.tab_voiture[voiture].orientation==HORIZONTALE)
		nouvelle_situation.tab_voiture[voiture].c+=deplacement;
	else
		nouvelle_situation.tab_voiture[voiture].l+=deplacement;
	
	return nouvelle_situation;
}
								
/* idée du premier main : on initialise un vecteur de situation vide,
on lit le fichier puzzle.txt -> situation_dynamique de base,
on met dans le vecteur la situation contenu dans la situation dynamique
tq le jeu n'est pas fini :
|	generer situation aleatoire
|	si situation généré n'est pas dans le vecteur on ajoute dans le vecteur
|_*/