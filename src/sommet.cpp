#include "sommet.hpp"


Sommet::Sommet(Situation s){
	valeur=s;
}

void Sommet::ajouter_arrete(Arrete a){
	suivant.push_back(a);
}

bool Sommet::operator==(Sommet s){
	return s.valeur==valeur;
}