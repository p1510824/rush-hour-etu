#include "graphe.hpp"

void Graphe::ajouter(Sommet s){
	vect_sommet.push_back(s);
}

int Graphe::rechercher(Sommet s){
	for(int i=0;i<vect_sommet.size();i++){
		if(vect_sommet[i]==s)
			return i;
	}
	return -1;
}


int Graphe::rechercher_pred(int ind_sommet){
	for(int i=0;i<vect_sommet.size();i++){
		for(int j=0;j<vect_sommet[i].suivant.size();j++){
			if (vect_sommet[i].suivant[j].ind_suivant==ind_sommet)
				return i;
		}
	}
	//on est pas sensé arriver la 
	return 0;
}
			