#ifndef _VOITURE_HPP_
#define _VOITURE_HPP_

#define VERTICALE 0
#define HORIZONTALE 1
struct Voiture {
    int c ; //colonne
    int l ; //ligne
    int longueur ;
    int orientation ;

    void initialiser(int _c, int _l, int _longueur, int _orientation);
	void egal(Voiture v);
	bool est_egal(Voiture v);
};


#endif