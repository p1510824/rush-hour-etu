#include "situation.hpp"
#include "voiture.hpp"




Situation::Situation() // constructeur par défaut
{
	c_sortie=TAILLE_TERRAIN;
	l_sortie=0;
	card=1;
	tab_voiture[0].initialiser(0,0,2,1); // on met la voiture rouge en (0,0) longueur 2, horizontal
}
	
Situation::Situation(char* nom_fichier) // constructeur selon un fichier donné (avec format approprié)
{
	std::ifstream flux(nom_fichier, std::ios::in);
	if (flux){
		short s_x,s_y,v_x,v_y,v_l,v_o;
		card=0;
		char car;
		flux >> s_y >> s_x;
		c_sortie=s_x;
		l_sortie=s_y;
		flux.get(car);//pour absorber le \n
		flux.get(car);//pour absorber le \n (note : je ne sais pas exactement pourquoi ca ne marche pas si on le fait qu'une seule fois : /n n'est pas consiéré comme un caractère unique ?)
		std::cout << s_y <<s_x << std::endl;
		while (flux){
			flux >>v_y>> v_x >> v_l >> v_o ;
			if (v_x==0 && v_y==0 && v_l==0 && v_o==0 ){// pour gérer la fin de fichier, c'est bizarre et moche mais sinon la dernière ligne est répétée deux fois a la fin
				card++;
				break;
			}
			else {
				std::cout <<"voiture n°"<<card<<" :"<< v_y <<" "<< v_x<<" " << v_l<<" " << v_o << std::endl;
				Voiture v;
				v.initialiser(v_x,v_y,v_l,v_o);
				tab_voiture[card]=v;
				v_y=0;
				v_x=0;
				v_l=0;
				v_o=0;
				card++;
			}
		}
		card--;
	}
	else {
		std::cerr << "impossible d'ouvrir le fichier,echec creation de la situation ..." << std::endl;
		exit(-1);
	}
	std::cout << card<<std::endl;
}
	
void Situation::ecrire_situation_fichier(char* nom_fichier) //permet de stocker la situation dans un fichier txt
{
	std::ofstream flux(nom_fichier,std::ios::out);
	if(flux){
		flux << l_sortie<<" "<<c_sortie<<std::endl;
		for(int i=0;i<card;i++){
			flux<<tab_voiture[i].l<<" "<<tab_voiture[i].c<<" "<<tab_voiture[i].longueur<<" "<<tab_voiture[i].orientation<<std::endl;
		}
	}
	else 
		std::cerr <<"impossible d'ouvrir le fichier en ecriture.."<<std::endl;
}
	
void Situation::afficher(){
	//on rempli un tableau fictif qui simule la position des voitures
	char tab[TAILLE_TERRAIN][TAILLE_TERRAIN]={' '};// ligne / colonne 
	for (int i=0; i<card; i++) {
		if(tab_voiture[i].orientation==1) {//voiture horizontale
			for(int n=tab_voiture[i].c;n<tab_voiture[i].c+tab_voiture[i].longueur;n++)
				tab[tab_voiture[i].l][n]='a'+i;
		}
		else {//voitire verticale
			for(int n=tab_voiture[i].l;n<tab_voiture[i].l+tab_voiture[i].longueur;n++)
				tab[n][tab_voiture[i].c]='a'+i;
		}
	}
	//affichage :
	std::cout<<"--------"<<std::endl;
	for (int i=0; i<TAILLE_TERRAIN; i++){
		std::cout<<"|";
		for (int j=0; j<TAILLE_TERRAIN; j++) {
			std::cout<< tab[i][j] ;
			
		}
		if(i!=l_sortie)
			std::cout<<"|"<<std::endl;
		else 
			std::cout<< " "<<std::endl;
	}
	std::cout<<"--------"<<std::endl;
}

bool Situation::jeu_fini(){
	if(tab_voiture[0].orientation==HORIZONTALE){
		if (c_sortie==TAILLE_TERRAIN-1) //sortie vers la droite
			return (tab_voiture[0].c+tab_voiture[0].longueur-1 == c_sortie);
		else 								//sortie vers la gauche
			return (tab_voiture[0].c == c_sortie);
	}
	else {
		if (l_sortie==TAILLE_TERRAIN-1) //sortie vers le bas	
			return (tab_voiture[0].l+tab_voiture[0].longueur-1 == l_sortie);
		else								//sortie vers le haut
			return (tab_voiture[0].l== l_sortie);
	}
}
	
bool Situation::operator==(Situation s){// a changer par la suite dans l'implémentaton si possible, car utilisé très souvent dans l'algo principal , par exemple idée d'un calcul d'un checksum unique selon les voiture contenu dans le tableau de voiture d'une situation
	if (card != s.card|| c_sortie!=s.c_sortie || l_sortie!=s.l_sortie)
		return false;
	for(int i=0;i<s.card;i++){
		Voiture v1=tab_voiture[i];
		Voiture v2=s.tab_voiture[i];
		if( not v1.est_egal(v2))
			return false;
	}
	return true;
}

void Situation::operator=(Situation s){
	c_sortie=s.c_sortie;
	l_sortie=s.l_sortie;
	card=s.card;
	for (int i=0;i<card;i++)
		tab_voiture[i]=s.tab_voiture[i];
}
