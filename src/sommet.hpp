#ifndef _SOMMET_HPP_
#define _SOMMET_HPP_

#include "situation_dynamique.hpp"
#include "arrete.hpp"

class Sommet{
	public:
	Situation valeur; //l'etat du jeu que ce sommet représente
	std::vector<Arrete> suivant;//la liste de tout les sommet que l'on peut atteindre grace a ce sommet : selon la liste des coups possible qui est calculable avec les situations dynamique
	
	Sommet(Situation s);
	void ajouter_arrete(Arrete a);
	bool operator==(Sommet s);
};
#endif