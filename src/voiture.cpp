#include "voiture.hpp"

void Voiture::initialiser(int _c, int _l, int _longueur, int _orientation) {
    c = _c ;
    l = _l ;
    longueur = _longueur ;
    orientation = _orientation ;
}

void Voiture::egal(Voiture v) {
    c=v.c;
	l=v.l;
	longueur=v.longueur;
	orientation=v.orientation;
}

bool Voiture::est_egal(Voiture v) {
    return (c==v.c && l==v.l && longueur==v.longueur && orientation==v.orientation);
}