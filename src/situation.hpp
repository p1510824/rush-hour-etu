#ifndef _SITUATION_HPP_
#define _SITUATION_HPP_

#include <vector>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include "voiture.hpp"
#define NB_VOITURE 16
#define TAILLE_TERRAIN 6

class Situation {
	public:
	/* liste attributs */
	int c_sortie; // colonne de la sortie (commence à 0)
	int l_sortie; //ligne de la sortie (commence à 0)
	Voiture tab_voiture [NB_VOITURE]; //tableau contenant l'ensemble de nos voitures il y a au moins une voiture, celle à faire sortir du terrain à l'indice 0 
	int card; // nombre donnat le nombre de voiture dans tab_voiture
	
	/* liste méthodes */
	Situation(); // constructeur par défaut
	Situation(char* nom_fichier); // constructeur selon un fichier donné (avec format approprié)
	void ecrire_situation_fichier(char* nom_fichier); //permet de stocker la situation dans un fichier txt
	void afficher();
	bool jeu_fini(); // si la voiture rouge a atteint la sortie
	bool operator==(Situation s);// a changer par la suite dans l'implémentaton si possible, car utilisé très souvent dans l'algo principal , par exemple idée d'un calcul d'un checksum unique selon les voiture contenu dans le tableau de voiture d'une situation
	void operator=(Situation s);
};

#endif
