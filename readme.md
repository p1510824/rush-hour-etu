# PROJET RUSH HOUR
 Binome : SEN Abdurrahman (11510824), ROBERT François (11808712)  
 
### Compilation : 
__Makefile__ fournit dans le dossier "_src_", il y a deux executables, `test` et `calcule_solution` :  
afin de compiler l'executable `test` il faut décommenter les lignes indiquées et commenter les lignes concernant l'autre executable  
et inversement pour compiler `calcule_solution`.

### Executables : 

* __test__ :  
    Utilisation : `./src/test puzzle.txt`  
    À partir de la solution initiale décrite dans `puzzle.txt`, recherche une solution de manière aléatoire.  
    ![Exécution de test](https://forge.univ-lyon1.fr/p1510824/rush-hour-etu/raw/master/Capture.PNG)
* __calcule_solution__ :   
    Utilisation : `./src/calcule_solution puzzle.txt`  
    À partir de la solution initiale décrite dans `puzzle.txt`, essaie de réaliser un parcours en largeur du graphe pour trouver une solution avec le plus petit nombre de coups nécessaires.  
    ![Exécution de calcule_solution](https://forge.univ-lyon1.fr/p1510824/rush-hour-etu/raw/master/Capture2.PNG)
    Le nombre de coups trouvés est de 17.  


  
